-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29-Nov-2018 às 20:52
-- Versão do servidor: 10.1.37-MariaDB
-- versão do PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ticketgo`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `chamados`
--

CREATE TABLE `chamados` (
  `id_chamado` int(11) NOT NULL,
  `assunto` varchar(500) NOT NULL,
  `mensagem` varchar(1000) NOT NULL,
  `statuschamado` varchar(50) NOT NULL,
  `fila` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `chamados`
--

INSERT INTO `chamados` (`id_chamado`, `assunto`, `mensagem`, `statuschamado`, `fila`) VALUES
(1, 'Mouse', 'Mouse nao funciona', 'Aberto', 'Hardware'),
(2, 'Teclado', 'Teclado nao funciona', 'Aberto', 'Hardware'),
(3, 'Senha Facebook', 'Esqueci a senha do Facebook', 'Aberto', 'Software'),
(5, 'Impressora', 'Impressora nao imprime', 'Aberto', 'Software');

-- --------------------------------------------------------

--
-- Estrutura da tabela `clientes`
--

CREATE TABLE `clientes` (
  `id_cliente` int(11) NOT NULL,
  `nomecliente` varchar(50) NOT NULL,
  `sobrenomecliente` varchar(100) NOT NULL,
  `emailcliente` varchar(100) NOT NULL,
  `senhacliente` varchar(200) NOT NULL,
  `ativo` tinyint(1) NOT NULL,
  `tipo` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `clientes`
--

INSERT INTO `clientes` (`id_cliente`, `nomecliente`, `sobrenomecliente`, `emailcliente`, `senhacliente`, `ativo`, `tipo`) VALUES
(1, 'Maria', 'do Bairro', 'maria@hotmail.com', '1234', 1, 1),
(4, 'Carlos', 'Silva', 'joao@mail.com', 'joao@mail.com', 1, 0),
(5, 'Guilherme', 'Lindoso', 'gui@mail.com', 'mebeija', 1, 0),
(6, 'Bojack', 'Horseman', 'cavalo@pocoto.com', 'bebidasemulheres', 1, 0),
(9, 'Annie', 'Firebear', 'lolzin@mail.com', 'fogonasinimigas', 0, 0),
(11, 'James', 'Bond', '007@mail.com', '007', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chamados`
--
ALTER TABLE `chamados`
  ADD PRIMARY KEY (`id_chamado`);

--
-- Indexes for table `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_cliente`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chamados`
--
ALTER TABLE `chamados`
  MODIFY `id_chamado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
